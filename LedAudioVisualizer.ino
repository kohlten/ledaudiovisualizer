#include <PololuLedStrip.h>

const int led_speed = 1;
const int fade_amount = 1;

PololuLedStrip<12> ledStrip;

#define LED_COUNT 144
rgb_color colors[LED_COUNT];
int led_high = 144;

rgb_color color_blue;
rgb_color color_red;
rgb_color color_green;
rgb_color color_off;

unsigned long last_ping = 0;


void setup()
{
  Serial.begin(9600);
  Serial.println(fade_amount);
  color_blue.blue = 255;
  color_red.red = 255;
  color_green.green = 255;
  color_off.red = 0;
  color_off.green = 0;
  color_off.blue = 0;
}

void mode_zero_loop() {
  int red_pos;
  int blue_pos;
  int green_pos = 0;
  int third;

  if (led_high >= 3) {
    third = led_high / 3;
    red_pos = third * 2;
    blue_pos = third;
  }
  for (uint16_t i = 0; i < LED_COUNT; i++)
  {
    if (i < led_high) {
      if (led_high == 1) {
        colors[i] = color_green;
      } 
      else if (led_high == 2) {
        if (i < led_high - 1)
          colors[i] = color_green;
        else
          colors[i] = color_blue;
      } 
      else {
        if (i < blue_pos)
          colors[i] = color_green;
        else if (i >= blue_pos && i < red_pos)
          colors[i] = color_blue;
        else
          colors[i] = color_red;
      }
    } 
    else
      colors[i] = color_off;
  }
  if (millis() % led_speed == 0) {
    if (led_high - fade_amount > 0)
      led_high -= fade_amount;
    else
      led_high = 0;
  }
}

void loop()
{
  String data;
  char c;
  int i;
  int len;
  int parsed;

  len = Serial.available();
  if (len >= 3) {
    data = "";
    for (i = 0; i < len; i++) {
      c = Serial.read();
      if (!(c >= '0' && c <= '9')) {
        break;
      }
      data += c;
    }
    parsed = data.toInt();
    if (parsed <= LED_COUNT && parsed >= 0 && parsed > led_high)
      led_high = parsed;
  }
  mode_zero_loop();
  ledStrip.write(colors, LED_COUNT);
}



