package main

import (
	"errors"
	"fmt"
	ledAudio "gitlab.com/kohlten/ledaudiovisualizer/led_audio_binding"
	"math"
	"time"
)

type AudioVisualizer struct {
	connection   *ledAudio.Connection
	client       *Serial
	running      bool
	samples      []float64
	min          float64
	max          float64
	totalSamples uint64
}

// 44100 = Samples Per Second
// 44100 / 4 = Samples per 400 MS
// (44100 / 4) * 2 = Total bytes for 400 MS of samples
const totalSize = (44100 / 21) * 2

const sampleSize = 500

func calcRMS(data float64) float64 {
	return 0.707 * data
}

func calcAverage(data []float64) float64 {
	added := 0.0
	for i := 0; i < len(data); i++ {
		added += data[i]
	}
	return added / float64(len(data))
}

func convertRange(value, min1, max1, min2, max2 float64) float64 {
	range1 := max1 - min1
	if range1 == 0 {
		return min2
	} else {
		range2 := max2 - min2
		return (((value - min1) * range2) / range1) + min2
	}
}

func NewAudioVisualizer(sink, device string) (*AudioVisualizer, error) {
	connection := ledAudio.LaNewConnection(sink)
	if connection == nil {
		return nil, errors.New("failed to create LaConnection")
	}
	client, err := NewSerialConnection(device, 9600)
	if err != nil {
		return nil, err
	}
	av := new(AudioVisualizer)
	av.connection = connection
	av.client = client
	av.running = true
	av.min = math.MaxFloat64
	av.max = 0
	return av, nil
}

func (av *AudioVisualizer) AddSample(sample float64) {
	av.samples = append(av.samples, sample)
	if len(av.samples) > sampleSize {
		av.samples = av.samples[(len(av.samples)-1)-sampleSize : len(av.samples)]
	}
	max := 0.0
	for i := 0; i < len(av.samples); i++ {
		if av.samples[i] > max {
			max = av.samples[i]
		}
		if av.samples[i] < av.min {
			av.min = av.samples[i]
		}
	}
	av.max = max
}

func (av *AudioVisualizer) Run() {
	for av.running {
		st := time.Now().UnixNano() / int64(time.Millisecond)
		for ledAudio.LaGetLen(av.connection) < totalSize {
			time.Sleep(1 * time.Millisecond)
		}
		data := ledAudio.LaGetData(av.connection, totalSize)
		amplitudes := ledAudio.LaGetAmplitudes(data, totalSize)
		floatAmplitudes := make([]float64, len(amplitudes))
		for i := 0; i < len(amplitudes); i++ {
			floatAmplitudes[i] = calcRMS(float64(amplitudes[i]))
		}
		average := calcAverage(floatAmplitudes)
		av.AddSample(average)
		led := convertRange(average, av.min, av.max, 0, 144)
		if led != math.NaN() && int(led) > 0 && int(led) <= 144 {
			fmt.Println(int(led))
			err := av.client.SendNum(int(led))
			if err != nil {
				fmt.Println("Failed to send to the Arduino!")
				break
			}
			av.totalSamples++
		}
		if av.totalSamples == math.MaxUint64 - 1 {
			av.totalSamples = 0
		}
		et := time.Now().UnixNano() / int64(time.Millisecond)
		if et - st < 16 {
			remaining := 16 - (et - st)
			time.Sleep(time.Duration(remaining) * time.Millisecond)
		}
	}
}

func (av *AudioVisualizer) Free() {
	ledAudio.LaFree(av.connection)
	_ = av.client.Close()
}
