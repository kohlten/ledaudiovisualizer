import sys
import struct


def main():
    args = sys.argv
    if len(args) != 3:
        print("Usage: python parse_data.py <input_pcm_file> <output_file_name>")
        sys.exit(1)
    input_file = args[1]
    fd = open(input_file, 'r')
    data = fd.read()
    fd.close()
    output_file_name = args[2]
    fd = open(output_file_name, 'w')
    for i in range(0, len(data), 2):
        num = struct.unpack('@h', data[i:i+2])[0]
        fd.write(str(num) + ' ')
    fd.close()


if __name__ == '__main__':
    main()
