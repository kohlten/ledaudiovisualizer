//
// Created by kohlten on 11/4/19.
//

#include <glib-2.0/glib.h>
#include <unistd.h>
#include <stdio.h>

#include "led_audio.h"
#include "pcm_parser.h"

void print_amplitudes(uint8_t *data, size_t len) {
    uint16_t *amplitudes;
    size_t amplitudes_length;
    size_t i;

    amplitudes = la_get_amplitudes(data, len, &amplitudes_length);
    g_assert(amplitudes != NULL);
    for (i = 0; i < amplitudes_length; i++)
        printf("%hu\n", amplitudes[i]);
    free(amplitudes);
}

static void output(const void *p) {
    t_connection *connection;
    uint8_t *data;
    FILE *output;

    (void) p;
    connection = la_new_connection("alsa_output.usb-Gamdias_Technology_Co.__Ltd._Gaming_Headset-00.analog-stereo.monitor", 44100);
    g_assert(connection != NULL);
    output = fopen("output_data", "w");
    g_assert(output != NULL);
    //for (iter = 0; iter < 500; iter++) {
    while (la_get_len(connection) < 44100 * 2)
        usleep(1);
    data = la_get_data(connection, 44100 * 2);
    g_assert(data != NULL);
    print_amplitudes(data, 44100 * 2);
    printf("%p\n", data);
    fwrite(data, sizeof(uint8_t),44100 * 2, output);
    free(data);
    fclose(output);
    la_free(connection);
}

void run(void) {
    size_t i;
    t_connection *connection;
    uint8_t *data;
    FILE *output;

    connection = la_new_connection("alsa_output.usb-Gamdias_Technology_Co.__Ltd._Gaming_Headset-00.analog-stereo.monitor", 44100);
    g_assert(connection != NULL);
    output = fopen("output_data", "w");
    g_assert(output != NULL);
    for (i = 0; i < 10000; i++) {
        while (la_get_len(connection) < 44100 * 2)
            usleep(1);
        data = la_get_data(connection, 44100 * 2);
        g_assert(data != NULL);
        fwrite(data, sizeof(uint8_t),44100 * 2, output);
        free(data);
    }
    fclose(output);
    la_free(connection);
}

int main(void) {
    //output(NULL);
    run();
}
