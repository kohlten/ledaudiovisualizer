//
// Created by kohlten on 11/4/19.
//

#ifndef LED_AUDIO_LED_AUDIO_H
#define LED_AUDIO_LED_AUDIO_H

#include <pulse/simple.h>
#include <pulse/error.h>
#include <stdbool.h>

#define READ_SIZE 500

typedef struct s_connection {
    pa_simple *pa;
    bool running;
    pthread_t thread;
    uint8_t *data;
    size_t len;
    pthread_mutex_t thread_mutex;
} t_connection;

t_connection *la_new_connection(const char *sink, unsigned int rate);
uint8_t *la_get_data(t_connection *connection, size_t len);
size_t la_get_len(t_connection *connection);
void la_free(t_connection *connection);

#endif //LED_AUDIO_LED_AUDIO_H
