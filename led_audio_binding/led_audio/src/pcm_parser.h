//
// Created by kohlten on 11/6/19.
//

#ifndef LED_AUDIO_PCM_PARSER_H
#define LED_AUDIO_PCM_PARSER_H

#include <stdint.h>
#include <unistd.h>

uint16_t *la_get_amplitudes(uint8_t *data, size_t len, size_t *output_length);

#endif //LED_AUDIO_PCM_PARSER_H
