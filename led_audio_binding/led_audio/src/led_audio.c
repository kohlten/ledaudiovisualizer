//
// Created by kohlten on 11/4/19.
//

#include "led_audio.h"

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>


void *la_loop(void *p) {
    t_connection *connection;
    uint8_t buff[READ_SIZE];
    int err;
    size_t i, j;
    uint8_t *new;

    connection = (t_connection *)p;
    while (connection->running) {
        pthread_mutex_lock(&connection->thread_mutex);
        err = 0;
        pa_simple_read(connection->pa, buff, READ_SIZE, &err);
        if (err != 0) {
            printf("Failed to read! Error: %s Exiting\n", pa_strerror(err));
            break;
        }
        new = malloc((connection->len + READ_SIZE) * sizeof(uint8_t));
        if (!new) {
            puts("Failed to allocate more data");
            break;
        }
        for (i = 0; i < connection->len; i++)
            new[i] = connection->data[i];
        for (j = 0; j < READ_SIZE; i++, j++)
            new[i] = buff[j];
        free(connection->data);
        connection->data = new;
        connection->len += READ_SIZE;
        pthread_mutex_unlock(&connection->thread_mutex);
        usleep(1);
    }
    return NULL;
}

t_connection *la_new_connection(const char *sink, unsigned int rate) {
    t_connection *connection;
    pa_sample_spec ss;
    int err;

	ss.rate = rate;
	ss.format = PA_SAMPLE_S16LE;
    ss.channels = 1;
    connection = malloc(sizeof(t_connection));
    if (!connection)
        return NULL;
    memset(connection, 0, sizeof(t_connection));
    connection->running = true;
    connection->data = NULL;
    pthread_mutex_init(&connection->thread_mutex, NULL);
    connection->pa = pa_simple_new(NULL, "led_audio_1", PA_STREAM_RECORD, sink, "record", &ss, NULL, NULL, &err);
    if (!connection->pa) {
        printf("Failed to create pa client with error %s\n", pa_strerror(err));
        goto fail;
    }
    pthread_create(&connection->thread, NULL, la_loop, connection);
    return connection;
    fail:
        if (connection->data)
            free(connection->data);
        free(connection);
        return NULL;
}

uint8_t *la_get_data(t_connection *connection, size_t len) {
    uint8_t *data;
    uint8_t *old;
    size_t i, j;

    if (len > connection->len)
        return NULL;
    pthread_mutex_lock(&connection->thread_mutex);
    data = malloc(len);
    for (i = 0; i < len; i++)
        data[i] = connection->data[i];
    old = connection->data;
    connection->data = malloc(connection->len - len);
    for (j = 0; i < connection->len; j++, i++)
        connection->data[j] = old[i];
    connection->len = connection->len - len;
    free(old);
    pthread_mutex_unlock(&connection->thread_mutex);
    return data;
}

size_t la_get_len(t_connection *connection) {
    return connection->len;
}

void la_free(t_connection *connection) {
    connection->running = false;
    pthread_join(connection->thread, NULL);
    if (connection->data)
        free(connection->data);
    pa_simple_free(connection->pa);
    pthread_mutex_destroy(&connection->thread_mutex);
    free(connection);
}

