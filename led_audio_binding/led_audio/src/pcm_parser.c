//
// Created by kohlten on 11/6/19.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

uint64_t la_get_waves_length(const int16_t *data, size_t len) {
    uint64_t waves;
    bool negative;
    size_t i;

    waves = 0;
    negative = false;
    for (i = 0; i < len; i++) {
        if (data[i] > 0 || data[i] < 0) {
            if (data[i] > 0)
                negative = false;
            else if (data[i] < 0)
                negative = true;
            break;
        }
    }
    for (; i < len; i++) {
        if (!negative && data[i] < 0) {
            negative = true;
            waves += 1;
        } else if (negative && data[i] > 0) {
            negative = false;
            waves += 1;
        }
    }
    return waves / 2;
}

int16_t *la_parse_data(uint8_t *data, size_t len, size_t *output_length) {
    int16_t *output;
    int16_t tmp;
    size_t i;
    size_t j;

    output = malloc((len / 2) * sizeof(int16_t));
    if (!output)
        return NULL;
    j = 0;
    for (i = 0; i < len; i += 2) {
        memcpy(&tmp, &data[i], sizeof(int16_t));
        output[j] = tmp;
        j++;
    }
    *output_length = j;
    return output;
}

uint16_t *la_parse_amplitudes(const int16_t *data, size_t len, size_t waves) {
    uint16_t *output;
    uint16_t max;
    size_t i;
    size_t j;
    bool did_once;

    output = malloc(waves * sizeof(uint16_t));
    if (!output)
        return NULL;
    max = 0;
    j = 0;
    did_once = false;
    for (i = 0; i < len;) {
        if (data[i] > 0) {
            if (data[i] > max)
                max = data[i];
            i++;
            did_once = true;
        } else if (did_once) {
            did_once = false;
            if (j < waves)
                output[j] = max;
            else
                break;
            max = 0;
            j++;
            while (data[i] < 0 && i < len)
                i++;
        } else
            i++;
    }
    return output;
}

uint16_t *la_get_amplitudes(uint8_t *data, size_t len, size_t *output_length) {
    uint16_t *output;
    uint64_t waves_length;
    int16_t *parsed_data;
    uint64_t parsed_length;


    parsed_data = la_parse_data(data, len, &parsed_length);
    if (!parsed_data)
        return NULL;
    waves_length = la_get_waves_length(parsed_data, parsed_length);
    output = la_parse_amplitudes(parsed_data, parsed_length, waves_length);
    if (!output) {
        free(parsed_data);
        return NULL;
    }
    free(parsed_data);
    *output_length = waves_length;
    return output;
}