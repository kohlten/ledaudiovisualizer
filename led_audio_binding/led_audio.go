package led_audio

// #cgo LDFLAGS: -L. -lpthread -lled_audio
// #cgo CFLAGS: -Iled_audio/src
// #cgo pkg-config: libpulse-simple
// #include <stdlib.h>
// #include "led_audio.h"
import "C"
import "unsafe"

type Connection = C.struct_s_connection


func LaNewConnection(sink string, rate uint32) *Connection {
	cSink := C.CBytes([]byte(sink))
	connection := C.la_new_connection((*C.char)(cSink), C.uint(rate))
	return connection
}

func LaGetData(connection *Connection, len uint64) []byte {
	cData := C.la_get_data(connection, C.size_t(len))
	if cData == nil {
		return nil
	}
	data := C.GoBytes(unsafe.Pointer(cData), C.int(len))
	C.free(unsafe.Pointer(cData))
	return data
}

func LaGetLen(connection *Connection) uint64 {
	return uint64(C.la_get_len(connection))
}

func LaFree(connection *Connection) {
	C.la_free(connection)
}



