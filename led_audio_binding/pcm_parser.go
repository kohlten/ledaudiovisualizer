package led_audio

// #cgo LDFLAGS: -lpthread -Lled_audio/build -lled_audio
// #cgo CFLAGS: -Iled_audio/src
// #cgo pkg-config: libpulse-simple
// #include <stdlib.h>
// #include "pcm_parser.h"
import "C"
import (
	"reflect"
	"unsafe"
)

func LaGetAmplitudes(data []byte, len uint64) []uint16 {
	cArray := unsafe.Pointer(&data[0])
	cLen := C.size_t(0)
	cData := C.la_get_amplitudes((*C.uchar)(cArray), C.size_t(len), &cLen)
	output := make([]uint16, 0)
	sh := (*reflect.SliceHeader)(unsafe.Pointer(&output))
	sh.Cap = int(cLen)
	sh.Len = int(cLen)
	sh.Data = uintptr(unsafe.Pointer(cData))
	outputCopy := make([]uint16, int(cLen))
	for i := 0; i < int(cLen); i++ {
		outputCopy[i] = output[i]
	}
	C.free(unsafe.Pointer(cData))
	return outputCopy
}