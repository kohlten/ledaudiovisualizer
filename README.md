This is just a simple audio visualizer using an Arduino and pulseaudio. 

The general idea on how this works is the C library gets PCM data from pulseaudio. 
It then parses that PCM data in golang and sends a number to the Arduino through serial to update the strip.

If you want to use this code, you will need to setup an arduino with an addressable RGB strip. You will also need the Arduino IDE.

After this you will need to download the library https://github.com/pololu/pololu-led-strip-arduino and add it to your Arduino libraries.


When that is all done, you will then need to change the arduino script (LedAudioVisualizer.ino) to follow your RBG strip. For me I had an RGB strip with 144 lights, so if you have more or less than that, you will need to chnage the LED_COUNT variable to the amount of LEDs you have.


After that is done, you should then be able to build and run the Arduino script. To check if it works just open the serial monitor under tools and send a number formated with zeros in the front (000 for 0).
Your RGB strip should light up and slowly fade away.

Then you can build the golang script. To build it you are first going to need to run 'go get github.com/tarm/serial'. After that you can run 'sh build.sh'. If that runs successfully, you then should see a executable called LedAudioVisualizer.

To run the program you first need the sink you want to listen from. To get this you just need to run 'LANG=C pactl list | grep -A2 'Source #' | grep 'Name: ' | cut -d" " -f2'.
Each one that shows up is a device you can record from. The output ones are the ones that end with '.monitor'.

Then you need to get the device that your Arduino serial is on. It should be '/dev/ttyACMX'. X being a number.

After getting those two things, you can run './LedAudioVisualizer sink device' to get it started. Replacing sink with the sink you chose, and the device from the previous step.