package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage: ./LedAudioVisualizer <sink> <device>")
		os.Exit(1)
	}
	sink := os.Args[1]
	device := os.Args[2]
	av, err := NewAudioVisualizer(sink, device)
	if err != nil {
		fmt.Println("Failed to create audio visualizer with error: ", err)
		os.Exit(1)
	}
	// If the person presses Ctrl-C or ends the program, make sure that we free
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		_ = <-signals
		av.running = false
	}()
	av.Run()
	av.Free()
}

