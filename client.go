package main

import (
	"github.com/tarm/serial"
	"strconv"
)

type Serial struct {
	fd *serial.Port
}

func NewSerialConnection(name string, baud int) (*Serial, error) {
	config := &serial.Config{
		Name:        name,
		Baud:        baud,
	}
	fd, err := serial.OpenPort(config)
	if err != nil {
		return nil, err
	}
	s := new(Serial)
	s.fd = fd
	return s, nil
}

func (s *Serial) SendNum(num int) error {
	if num < 0 {
		num = 0
	}
	strNum := strconv.FormatUint(uint64(num), 10)
	for len(strNum) < 3 {
		strNum = "0" + strNum
	}
	_, err := s.fd.Write([]byte(strNum))
	return err
}

func (s *Serial) Close() error {
	return s.fd.Close()
}
